import os
list = []

class todo:
	def __init__(self, name, tag="", proj=""):
		self.pri = ""
		self.name = name

		if(tag == ""):
			self.tag = []
		else:
			self.tag = [tag]

		if(proj == ""):
			self.proj = []
		else:
			self.proj = [proj]

		list.append(self)


	def __str__(self):

		proj = ""
		tag = ""
		pri = ""

		for i in self.proj:
			proj += "+%s " % i

		for j in self.tag:
			tag += "@%s " % j

		if(self.pri != ""):
			pri = "(%s) " % self.pri


		return "- %s%s %s%s" % (pri, self.name , tag, proj)

	def for_file(self):

		proj = ""
		tag = ""
		pri = ""

		for i in self.proj:
			proj += "+*%s* " % i

		for j in self.tag:
			tag += "@*%s* " % j

		if(self.pri != ""):
			pri = "(%s) " % self.pri

		return "- %s**%s** %s%s" % (pri, self.name , tag, proj)

	def set_pri(self, pri):
		self.pri = pri

	def add_proj(self, proj):
		self.proj.append(proj)

	def add_tag(self, tag):
		self.tag.append(tag)



def write_file(todo):
	path = "~/Dropbox/Notes/Todo.md"
	string = ""

	for i in list:
		string += "%s \n" % i.for_file()

	file = open(os.path.expanduser(path), 'w')

	file.write(string)

	file.close()
