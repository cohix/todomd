import todo

def main():
	print "Welcome to Todomd.py!"

	todo1 = todo.todo("Todo 1", "tag1", "proj1")

	todo1.add_proj("proj2")
	todo1.add_tag("tag2")

	print todo1

	todo2 = todo.todo("Todo 2")

	todo2.add_proj("proj3")
	todo2.add_tag("tag3")

	todo2.set_pri("B")

	print todo2

	todo.write_file(todo1)
	todo.write_file(todo2)

main()
